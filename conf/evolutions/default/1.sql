# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table add_on_items (
  id                        bigint not null,
  menu_id                   bigint,
  add_on_code               varchar(255),
  add_on_name               varchar(255),
  add_on_type               varchar(255),
  add_on_price              decimal(38),
  add_on_happy_hour_price   decimal(38),
  add_on_potential_price    decimal(38),
  add_on_account            varchar(255),
  constraint pk_add_on_items primary key (id))
;

create table advertisement (
  id                        bigint not null,
  ad_url                    varchar(255),
  date                      timestamp,
  constraint pk_advertisement primary key (id))
;

create table eventtypes (
  id                        bigint not null,
  event_type                varchar(255),
  event_type_description    varchar(255),
  constraint pk_eventtypes primary key (id))
;

create table Events (
  id                        bigint not null,
  event_name                varchar(255),
  event_date                timestamp,
  no_of_passes              integer,
  event_type_id             bigint,
  constraint pk_Events primary key (id))
;

create table menu_item_classifiers (
  id                        bigint not null,
  menu_id                   bigint,
  classifier_code           varchar(255),
  classifier_name           varchar(255),
  classifier_image_url      varchar(255),
  constraint pk_menu_item_classifiers primary key (id))
;

create table menus (
  id                        bigint not null,
  menu_code                 varchar(255),
  menu_name                 varchar(255),
  sub_menu_code             varchar(255),
  sub_menu_name             varchar(255),
  item_code                 varchar(255),
  item_name                 varchar(255),
  item_description          varchar(255),
  item_price                decimal(38),
  item_happy_hour_price     decimal(38),
  item_type                 varchar(255),
  item_image_url            varchar(255),
  item_potential_price      decimal(38),
  item_account              varchar(255),
  constraint pk_menus primary key (id))
;

create table Offers (
  id                        bigint not null,
  name                      varchar(255),
  description               varchar(255),
  time_left                 varchar(255),
  type                      varchar(255),
  constraint pk_Offers primary key (id))
;

create table orders (
  id                        bigint not null,
  user_order_id             bigint,
  menu_id                   bigint,
  quantity                  integer,
  quantity_type             varchar(255),
  item_price                decimal(38),
  item_tax                  decimal(38),
  item_total_price          decimal(38),
  constraint pk_orders primary key (id))
;

create table passpurchases (
  id                        bigint not null,
  table_no                  integer,
  user_id                   bigint,
  event_id                  bigint,
  attender_name             varchar(255),
  pass_type                 varchar(255),
  no_of_passes              integer,
  constraint pk_passpurchases primary key (id))
;

create table role (
  id                        bigint not null,
  role                      varchar(255),
  constraint pk_role primary key (id))
;

create table useroffers (
  id                        bigint not null,
  offer_id                  bigint,
  user_id                   bigint,
  constraint pk_useroffers primary key (id))
;

create table user_orders (
  id                        bigint not null,
  user_id                   bigint,
  offer_id                  bigint,
  event_id                  bigint,
  session_id                varchar(255),
  created_at                timestamp,
  status_at                 timestamp,
  constraint pk_user_orders primary key (id))
;

create table userregistration (
  id                        bigint not null,
  user_name                 varchar(255),
  password                  varchar(255),
  session_id                varchar(255),
  created_at                timestamp,
  status_at                 timestamp,
  role_id                   bigint,
  constraint pk_userregistration primary key (id))
;

create sequence add_on_items_seq;

create sequence advertisement_seq;

create sequence eventtypes_seq;

create sequence Events_seq;

create sequence menu_item_classifiers_seq;

create sequence menus_seq;

create sequence Offers_seq;

create sequence orders_seq;

create sequence passpurchases_seq;

create sequence role_seq;

create sequence useroffers_seq;

create sequence user_orders_seq;

create sequence userregistration_seq;

alter table add_on_items add constraint fk_add_on_items_menu_1 foreign key (menu_id) references menus (id);
create index ix_add_on_items_menu_1 on add_on_items (menu_id);
alter table Events add constraint fk_Events_eventTypes_2 foreign key (event_type_id) references eventtypes (id);
create index ix_Events_eventTypes_2 on Events (event_type_id);
alter table menu_item_classifiers add constraint fk_menu_item_classifiers_menu_3 foreign key (menu_id) references menus (id);
create index ix_menu_item_classifiers_menu_3 on menu_item_classifiers (menu_id);
alter table orders add constraint fk_orders_userOrder_4 foreign key (user_order_id) references user_orders (id);
create index ix_orders_userOrder_4 on orders (user_order_id);
alter table orders add constraint fk_orders_menu_5 foreign key (menu_id) references menus (id);
create index ix_orders_menu_5 on orders (menu_id);
alter table passpurchases add constraint fk_passpurchases_userRegister_6 foreign key (user_id) references userregistration (id);
create index ix_passpurchases_userRegister_6 on passpurchases (user_id);
alter table passpurchases add constraint fk_passpurchases_events_7 foreign key (event_id) references Events (id);
create index ix_passpurchases_events_7 on passpurchases (event_id);
alter table useroffers add constraint fk_useroffers_offers_8 foreign key (offer_id) references Offers (id);
create index ix_useroffers_offers_8 on useroffers (offer_id);
alter table useroffers add constraint fk_useroffers_user_9 foreign key (user_id) references userregistration (id);
create index ix_useroffers_user_9 on useroffers (user_id);
alter table user_orders add constraint fk_user_orders_user_10 foreign key (user_id) references userregistration (id);
create index ix_user_orders_user_10 on user_orders (user_id);
alter table user_orders add constraint fk_user_orders_offers_11 foreign key (offer_id) references Offers (id);
create index ix_user_orders_offers_11 on user_orders (offer_id);
alter table user_orders add constraint fk_user_orders_events_12 foreign key (event_id) references Events (id);
create index ix_user_orders_events_12 on user_orders (event_id);
alter table userregistration add constraint fk_userregistration_role_13 foreign key (role_id) references role (id);
create index ix_userregistration_role_13 on userregistration (role_id);



# --- !Downs

drop table if exists add_on_items cascade;

drop table if exists advertisement cascade;

drop table if exists eventtypes cascade;

drop table if exists Events cascade;

drop table if exists menu_item_classifiers cascade;

drop table if exists menus cascade;

drop table if exists Offers cascade;

drop table if exists orders cascade;

drop table if exists passpurchases cascade;

drop table if exists role cascade;

drop table if exists useroffers cascade;

drop table if exists user_orders cascade;

drop table if exists userregistration cascade;

drop sequence if exists add_on_items_seq;

drop sequence if exists advertisement_seq;

drop sequence if exists eventtypes_seq;

drop sequence if exists Events_seq;

drop sequence if exists menu_item_classifiers_seq;

drop sequence if exists menus_seq;

drop sequence if exists Offers_seq;

drop sequence if exists orders_seq;

drop sequence if exists passpurchases_seq;

drop sequence if exists role_seq;

drop sequence if exists useroffers_seq;

drop sequence if exists user_orders_seq;

drop sequence if exists userregistration_seq;

