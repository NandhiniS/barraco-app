package controllers;

import java.util.ArrayList;
import java.util.List;

import models.transactions.Orders;
import models.transactions.UserOrders;

import org.codehaus.jackson.JsonNode;

import play.libs.Json;
import play.mvc.Http;
import play.mvc.Http.Context;
import play.mvc.Http.Request;
import play.mvc.Result;

import com.api.tabletop.dto.BillDTO;

public class BillController extends APIBaseController {

	public static Result getCurrentBill() {

		Request request = Context.current().request();
		JsonNode asJson = request.body().asJson();

		Long userId = asJson.get("userId").getLongValue();
		String sessionId = asJson.get("sessionId").getTextValue();

		UserOrders findByUserAndSession = UserOrders.findByUserAndSession(userId, sessionId);
		List<Orders> findByUserOrderId = Orders.findByUserOrderId(findByUserAndSession.getId());

		BillDTO bill = new BillDTO(findByUserOrderId);
		JsonNode json = Json.toJson(bill);
		return JsonResponse(json, Http.Status.OK);
	}

	public static Result getOrderHistory() {

		Request request = Context.current().request();
		JsonNode asJson = request.body().asJson();

		Long userId = asJson.get("userId").getLongValue();

		List<UserOrders> findByUser = UserOrders.findByUser(userId);
		List<BillDTO> allOrders = new ArrayList<BillDTO>();
		for (UserOrders eachOrder : findByUser) {

			List<Orders> findByUserOrderId = Orders.findByUserOrderId(eachOrder.getId());
			allOrders.add(new BillDTO(findByUserOrderId));
		}

		JsonNode json = Json.toJson(allOrders);
		return JsonResponse(json, Http.Status.OK);
	}

}
