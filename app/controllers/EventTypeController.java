package controllers;

import java.util.List;

import models.masters.EventTypes;
import models.masters.Offers;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

public class EventTypeController extends Controller {

	static Form<EventTypes> eventTypeForm = Form.form(EventTypes.class);

	public static Result createEventTypes() {

		return ok(views.html.event_type.render(eventTypeForm, EventTypes.all()));
	}

	public static Result addEventTypes() {

		Form<EventTypes> filledForm = eventTypeForm.bindFromRequest();

		if (filledForm.hasErrors()) {
			flash().put("Error", "All Fields are mandatory!!!");
			return badRequest(views.html.event_type.render(eventTypeForm, EventTypes.all()));
		}

		EventTypes.submit(filledForm.get());
		flash().put("Message", "Added Successfully");

		return redirect(controllers.routes.EventTypeController.createEventTypes());

	}

	public static Result getEventByType() {

		Form<EventTypes> filledForm = eventTypeForm.bindFromRequest();
		String eventType = filledForm.get().getEventType();
		List<EventTypes> eventTypes = null;
		if (eventType == "") {
			eventTypes = EventTypes.all();
		} else {
			eventTypes = EventTypes.getEventsByType(eventType);
		}
		return ok(Json.toJson(eventTypes));

	}

}
