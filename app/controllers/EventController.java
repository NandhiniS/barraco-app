package controllers;

import models.masters.EventTypes;
import models.masters.Events;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;

public class EventController extends Controller {

	static Form<Events> eventForm = Form.form(Events.class);

	private static final Logger logger = LoggerFactory.getLogger(EventController.class);

	public static Result createEvent() {

		return ok(views.html.events.render(eventForm, Events.all(), EventTypes.all()));
	}

	public static Result addEvent() {

		Form<Events> filledForm = eventForm.bindFromRequest();

		if (filledForm.hasErrors()) {
			flash().put("Error", "All Fields are mandatory!!!");
			return badRequest(views.html.events.render(eventForm, Events.all(), EventTypes.all()));
		}

		Events.submit(filledForm.get());
		Events data = filledForm.get();
		logger.debug(data.getEventName());

		flash().put("Message", "Added Successfully");

		return redirect(controllers.routes.EventController.createEvent());

	}

}
