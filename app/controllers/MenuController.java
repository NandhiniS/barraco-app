package controllers;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import models.masters.menus.Menus;

import org.codehaus.jackson.JsonNode;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.data.Form;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Http.Context;
import play.mvc.Http.Request;
import play.mvc.Result;

import com.api.tabletop.dto.MenusDTO;
import com.api.touche.response.Menu;
import com.api.touche.response.Menu.OutletMenus;
import com.api.touche.response.Menu.OutletMenus.SubMenus;
import com.api.touche.response.Menu.OutletMenus.SubMenus.MenuItems;
import com.api.touche.response.Menu.OutletMenus.SubMenus.MenuItems.AddOnItems;
import com.api.touche.response.Menu.OutletMenus.SubMenus.MenuItems.MenuItemClassifiers;
import com.mapper.AddOnMapper;
import com.mapper.MenuMapper;
import com.utils.JsonUtils;

public class MenuController extends APIBaseController {

	private static final Logger logger = LoggerFactory.getLogger(MenuController.class);
	public static Form<Object> inputForm = Form.form(Object.class);

	public static Result globalConfig() throws Exception {
		// Form<Object> fromRequest = inputForm.bindFromRequest();
		// if (fromRequest.hasErrors()) {
		// logger.debug("errors");
		// }

		String query1 = "RequestString={'PropertyId':'DEMO1','AppKey':'TEST','Timestamp':'12032013','DeviceId':'DEVICE1'}";
		String query2 = "RequestString={'PropertyId':'DEMO1','AppKey':'TEST','Timestamp':'12032013','DeviceId':'DEVICE1','OutletCode':'BR'}";
		String detailsURL = "http://192.168.1.115/ToucheService/touche/GetOutletDetails?" + query2;
		String url = "http://192.168.1.115/ToucheService/touche/GetGlobalConfiguration?" + query1;

		URL obj = new URL(detailsURL);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		// print result
		System.out.println(response.toString());
		Menu fromJson = JsonUtils.bindFromJSON(response.toString());
		List<OutletMenus> outletMenus = fromJson.getOutletMenus();
		for (OutletMenus eachOutletMenu : outletMenus) {
			List<SubMenus> subMenus = eachOutletMenu.getSubMenus();
			for (SubMenus eachSubMenu : subMenus) {
				List<MenuItems> menuItems = eachSubMenu.getMenuItems();
				for (MenuItems eachMenuItem : menuItems) {
					ModelMapper modelMapper = new ModelMapper();
					modelMapper.addMappings(new MenuMapper());
					Menus eachMenu = modelMapper.map(eachMenuItem, Menus.class);
					eachMenu.setMenuCode(eachOutletMenu.getMenuCode());
					eachMenu.setMenuName(eachOutletMenu.getMenuName());
					eachMenu.setSubMenuCode(eachSubMenu.getSubMenuCode());
					eachMenu.setSubMenuName(eachSubMenu.getSubMenuName());
					eachMenu.setItemImageUrl(eachMenuItem.getItemImageUrl());
					logger.debug("" + eachMenu.getItemName());

					eachMenu.saveModel();

					List<AddOnItems> addOnItems = eachMenuItem.getAddOnItems();
					for (AddOnItems eachAddOn : addOnItems) {
						ModelMapper addOnMap = new ModelMapper();
						addOnMap.addMappings(new AddOnMapper());
						models.masters.menus.AddOnItems addOns = addOnMap.map(eachAddOn, models.masters.menus.AddOnItems.class);
						addOns.setMenu(eachMenu);
						addOns.saveModel();
					}

					List<MenuItemClassifiers> menuItemClassifiers = eachMenuItem.getMenuItemClassifiers();
					for (MenuItemClassifiers eachClassifier : menuItemClassifiers) {
						ModelMapper classifierMap = new ModelMapper();
						classifierMap.addMappings(new AddOnMapper());
						models.masters.menus.MenuItemClassifier classifiers = classifierMap.map(eachClassifier, models.masters.menus.MenuItemClassifier.class);
						classifiers.setMenu(eachMenu);
						classifiers.saveModel();
					}
				}
			}
		}
		// System.out.println(fromJson.getOutletMenus().get(0).getSubMenus().get(0).getMenuItems().get(0).getItemName());
		JsonNode parse = Json.parse(response.toString());
		return JsonResponse(parse, Http.Status.OK);
	}

	public static Result getMenuList() throws Exception {
		Request request = Context.current().request();
		JsonNode asJson = request.body().asJson();
		String menuCode = asJson.get("menuCode").getTextValue();
		List<Menus> findBySubMenu = Menus.findByMenuCode(menuCode);

		MenusDTO menusDTO = new MenusDTO(findBySubMenu);
		JsonNode json = Json.toJson(menusDTO);
		return JsonResponse(json, Http.Status.OK);

		// return JsonResponse(findBySubMenu, Http.Status.OK);

		// ObjectNode result = Json.newObject();
		// result.putPOJO("", findBySubMenu);
		//
		// File fi = new File(findBySubMenu.get(0).getProductFilePath());
		// byte[] fileContent = Files.readAllBytes(fi.toPath());
		// result.put("image", fileContent);
		//
		// return JsonResponse(result, Http.Status.OK);
	}

	public static Result getSubMenuList() throws Exception {
		Request request = Context.current().request();
		JsonNode asJson = request.body().asJson();
		String menuCode = asJson.get("subMenuCode").getTextValue();
		List<Menus> findBySubMenu = Menus.findBySubMenuCode(menuCode);

		MenusDTO menusDTO = new MenusDTO(findBySubMenu);
		JsonNode json = Json.toJson(menusDTO);
		return JsonResponse(json, Http.Status.OK);

		// findBySubMenu.get(0).setItemImageUrl("http://192.168.1.115/ToucheImages/Images/NIDRA/items/jonie_gloden.png");
		// findBySubMenu.get(1).setItemImageUrl("http://192.168.1.115/ToucheImages/Images/NIDRA/items/johnie_black.png");
		// findBySubMenu.get(3).setItemImageUrl("http://192.168.1.115/ToucheImages/Images/NIDRA/items/Powers.png");
		// return JsonResponse(findBySubMenu, Http.Status.OK);
	}

	// public static Result createOrder() throws Exception {
	// Request request = Context.current().request();
	// JsonNode asJson = request.body().asJson();
	// String menuCode = asJson.get("subMenuCode").getTextValue();
	// List<Menus> findBySubMenu = Menus.findBySubMenuCode(menuCode);
	//
	// return JsonResponse(findBySubMenu, Http.Status.OK);
	// }

}
