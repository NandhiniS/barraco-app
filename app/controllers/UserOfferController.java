package controllers;

import models.masters.Offers;
import models.transactions.UserOffers;
import models.transactions.UserRegistration;
import play.mvc.Controller;
import play.mvc.Result;

public class UserOfferController extends Controller {

	public static Result userOffers() {

		return ok(views.html.user_offers.render(UserOffers.all()));
	}
}
