package controllers;

import java.util.UUID;

import models.masters.Role;
import models.transactions.UserRegistration;

import org.codehaus.jackson.node.ObjectNode;

import play.data.Form;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;

import com.constants.AppConstants;

public class UserController extends APIBaseController {

	static Form<UserRegistration> userRegistrationForm = Form.form(UserRegistration.class);

	public static Result newUserRegistration() {

		return ok(views.html.newUserRegistration.render(userRegistrationForm, Role.all()));
	}

	public static Result saveNewUser() {
		Form<UserRegistration> filledForm = userRegistrationForm.bindFromRequest();

		if (filledForm.hasErrors()) {
			flash().put("Error", "All Fields are mandatory!!!");
			return badRequest(views.html.newUserRegistration.render(userRegistrationForm, Role.all()));
		}
		UserRegistration userRegistration = filledForm.get();
		userRegistration.submit();
		flash().put("Message", "Added Successfully");

		return redirect(controllers.routes.UserController.newUserRegistration());
	}

	public static Result createNewTableTopUser() {
		Form<UserRegistration> filledForm = userRegistrationForm.bindFromRequest();
		ObjectNode result = Json.newObject();
		UserRegistration userRegistration = filledForm.get();
		UserRegistration findByName = UserRegistration.findByName(userRegistration.getUserName());
		if (findByName != null) {
			result.put("Error", "User Name Already Exists");
			return JsonResponse(result, Http.Status.UNAUTHORIZED);
		} else {

			userRegistration.setRole(Role.findByRoleName(AppConstants.CUSTOMER));
			userRegistration.submit();
		}

		result.put("UserId", userRegistration.getId());
		result.put("SessionId", userRegistration.getSessionId());
		return JsonResponse(result, Http.Status.OK);
	}

	public static Result loginTableTopUser() {
		Form<UserRegistration> filledForm = userRegistrationForm.bindFromRequest();
		ObjectNode result = Json.newObject();
		UserRegistration findByName = UserRegistration.authorize(filledForm.get());
		if (findByName == null) {
			result.put("Error", "Invalid User Name / Password");
			return JsonResponse(result, Http.Status.UNAUTHORIZED);
		}

		findByName.setSessionId(UUID.randomUUID().toString().replaceAll("-", ""));
		findByName.updateModel();
		result.put("UserId", findByName.getId());
		result.put("SessionId", findByName.getSessionId());
		return JsonResponse(result, Http.Status.OK);
	}
}
