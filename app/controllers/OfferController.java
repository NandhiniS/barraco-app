package controllers;

import java.util.ArrayList;
import java.util.List;

import models.masters.Offers;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.data.Form;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Http.Context;
import play.mvc.Http.Request;
import play.mvc.Result;

import com.api.tabletop.dto.OffersDTO;

public class OfferController extends APIBaseController {

	static Form<Offers> offerForm = Form.form(Offers.class);
	private static final Logger logger = LoggerFactory.getLogger(OfferController.class);

	public static Result createOffer() {
		return ok(views.html.offers.render(offerForm, Offers.findAll()));
	}

	public static Result addOffer() {

		Form<Offers> filledForm = offerForm.bindFromRequest();

		if (filledForm.hasErrors()) {
			flash().put("Error", "All Fields are mandatory!!!");
			return badRequest(views.html.offers.render(offerForm, Offers.findAll()));
		}

		Offers.submit(filledForm.get());
		flash().put("Message", "Added Successfully");

		return redirect(controllers.routes.OfferController.createOffer());

	}

	public static Result getOfferByType() {

		Form<Offers> filledForm = offerForm.bindFromRequest();
		String offerType = filledForm.get().getType();
		List<Offers> offers = null;
		if (offerType == "") {
			offers = Offers.findAll();
		} else {
			offers = Offers.getOffersByType(offerType);
		}
		return ok(Json.toJson(offers));

	}

	public static Result getOffersList() throws Exception {
		Request request = Context.current().request();
		JsonNode asJson = request.body().asJson();
		String offerType = asJson.get("offerType").getTextValue();
		List<Offers> offers = new ArrayList<Offers>();
		if (StringUtils.equals("all", offerType)) {
			offers = Offers.findAll();
		} else {
			offers = Offers.getOffersByType(offerType);
		}
		OffersDTO offersDTO = new OffersDTO(offers);
		JsonNode json = Json.toJson(offersDTO);
		return JsonResponse(json, Http.Status.OK);
	}

}
