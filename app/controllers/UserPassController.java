package controllers;

import models.transactions.PassPurchases;
import play.mvc.Controller;
import play.mvc.Result;

public class UserPassController extends Controller {

	public static Result userPasses() {

		return ok(views.html.user_passpurchases.render(PassPurchases.all()));
	}
}
