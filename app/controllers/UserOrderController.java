package controllers;

import java.util.List;

import models.masters.menus.Menus;
import models.transactions.Orders;
import models.transactions.UserOrders;
import models.transactions.UserRegistration;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.data.Form;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;

import com.api.tabletop.dto.PreviousOrderDTO;

public class UserOrderController extends APIBaseController {

	private static final Logger logger = LoggerFactory.getLogger(MenuController.class);
	public static Form<UserOrders> orderForm = Form.form(UserOrders.class);

	public static Result createOrder() throws Exception {

		Form<UserOrders> fromRequest = (Form<UserOrders>) orderForm.bindFromRequest();
		UserOrders orderRequest = fromRequest.get();
		List<Orders> orders = orderRequest.getOrders();

		UserRegistration user = orderRequest.getUser();
		UserRegistration findById = UserRegistration.findById(user.getId());
		if (StringUtils.equals(orderRequest.getSessionId(), findById.getSessionId())) {
			// current session
			orderRequest = UserOrders.findById(orderRequest.getId());

		} else {
			// new session-- create order
			orderRequest.setId(null);
			orderRequest.setUser(findById);
			orderRequest.setSessionId(findById.getSessionId());
			logger.debug("username" + user.getUserName());
			orderRequest.saveModel();
		}

		// if (orderRequest.getId() != null) {
		// orderRequest = UserOrders.findById(orderRequest.getId());
		//
		// } else {
		// UserRegistration user = orderRequest.getUser();
		// orderRequest.setUser(UserRegistration.findById(user.getId()));
		// logger.debug("username" + user.getUserName());
		// orderRequest.saveModel();
		//
		// }

		for (Orders eachOrder : orders) {
			eachOrder.setMenu(Menus.findByItemCode(eachOrder.getMenu().getItemCode()));
			eachOrder.setUserOrder(orderRequest);
			eachOrder.saveModel();
		}
		ObjectNode result = Json.newObject();
		result.put("UserOrderId", orderRequest.getId());
		result.put("UserId", orderRequest.getUser().getId());
		result.put("SessionId", orderRequest.getUser().getSessionId());
		return JsonResponse(result, Http.Status.OK);

	}

	public static Result getPreviousOrders() throws Exception {
		Form<UserOrders> fromRequest = (Form<UserOrders>) orderForm.bindFromRequest();
		UserOrders orderRequest = fromRequest.get();

		// user order id
		// user id
		// session id
		List<Orders> findByUserOrderId = Orders.findByUserOrderId(orderRequest.getId());

		PreviousOrderDTO orderDTO = new PreviousOrderDTO(findByUserOrderId);
		JsonNode json = Json.toJson(orderDTO);
		return JsonResponse(json, Http.Status.OK);
		// return JsonResponse(findByUserOrderId, Http.Status.OK);
	}
}
