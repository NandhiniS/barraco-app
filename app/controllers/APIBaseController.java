package controllers;

import java.io.StringWriter;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;

import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

public class APIBaseController extends Controller {

	public static ObjectMapper mapper;
	static {
		mapper = new ObjectMapper();
		mapper.setSerializationConfig(mapper.getSerializationConfig().without(SerializationConfig.Feature.AUTO_DETECT_IS_GETTERS));
	}

	public static Result JsonResponse(Object obj, int response) {
		StringWriter writer = new StringWriter();
		try {
			mapper.writeValue(writer, obj);
		} catch (Exception e) {
			e.printStackTrace();
			return internalServerError("Server error! mapper.writeValue thrown - ");
		}

		switch (response) {
		case Http.Status.OK:
			return ok(writer.toString());
		case Http.Status.BAD_REQUEST:
			return badRequest(writer.toString());
		case Http.Status.UNAUTHORIZED:
			return unauthorized(writer.toString());
		default:
			return ok(writer.toString());
		}
	}
}
