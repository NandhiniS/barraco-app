package controllers;

import javax.persistence.Entity;
import javax.persistence.Table;

import models.masters.Advertisement;
import models.masters.EventTypes;
import models.masters.Role;
import models.transactions.UserRegistration;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;

@Entity
@Table(name = "advertisement")
public class AdvertisementController extends Controller {

	static Form<Advertisement> advertisementForm = Form.form(Advertisement.class);

	public static Result addNewAdvertisement() {

		return ok(views.html.advertisement.render(advertisementForm));
	}

	public static Result saveNewAdvertisement() {
		Form<Advertisement> filledForm = advertisementForm.bindFromRequest();

		if (filledForm.hasErrors()) {
			flash().put("Error", "All Fields are mandatory!!!");
			return badRequest(views.html.advertisement.render(advertisementForm));
		}

		String data = filledForm.get().getAdURL();
		System.out.println(data);
		String advertUrl = Advertisement.addPicture(data);
		System.out.println(advertUrl);

		Advertisement.submit(filledForm.get());
		flash().put("Message", "Added Successfully");

		return redirect(controllers.routes.AdvertisementController.addNewAdvertisement());
	}
}
