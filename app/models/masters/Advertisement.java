package models.masters;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import models.BaseModel;

@Entity
@Table(name = "advertisement")
public class Advertisement extends BaseModel {

	/** System generated Serial Version UID. */
	private static final long serialVersionUID = -2988118092110197157L;
	private String adURL;
	private Date date;

	/*
	 * private Time time; public Time getTime() { return time; } public void
	 * setTime(Time time) { this.time = time; }
	 */

	public String getAdURL() {
		return adURL;
	}

	public void setAdURL(String adURL) {
		this.adURL = adURL;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public static void submit(Advertisement advertisement) {
		// TODO Auto-generated method stub
		advertisement.save();
	}

	public static String addPicture(String data) {
		// TODO Auto-generated method stub
		String ADVERTISEMENT_URL = "/home/nandhini/Barroco-app/public/images/";
		String adUrl = ADVERTISEMENT_URL + data;

		return adUrl;

	}

}
