package models.masters;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import models.BaseModel;

@Entity
@Table(name = "eventtypes")
public class EventTypes extends BaseModel {

	/** System generated Serial Version UID. */
	private static final long serialVersionUID = -3842358100183082023L;
	private String eventType;
	private String eventTypeDescription;

	@OneToMany(mappedBy = "eventtypes", cascade = CascadeType.ALL)
	Events event;

	public Events getEvent() {
		return event;
	}

	public void setEvent(Events event) {
		this.event = event;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getEventTypeDescription() {
		return eventTypeDescription;
	}

	public void setEventTypeDescription(String eventTypeDescription) {
		this.eventTypeDescription = eventTypeDescription;
	}

	public static void submit(EventTypes eventTypes) {
		// TODO Auto-generated method stub
		eventTypes.saveModel();

	}

	public static Finder<Long, EventTypes> find = new Finder<Long, EventTypes>(Long.class, EventTypes.class);

	public static List<EventTypes> all() {
		// TODO Auto-generated method stub
		return find.all();
	}

	public static List<EventTypes> getEventsByType(String type) {
		// TODO Auto-generated method stub
		return find.where().eq("eventType", type).findList();
	}

}
