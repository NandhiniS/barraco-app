package models.masters;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.BaseModel;

@Entity
@Table(name = "Events")
public class Events extends BaseModel {

	/** System generated Serial Version UID. */
	private static final long serialVersionUID = 2546835174622387968L;
	private String eventName;
	private Date eventDate;
	private int noOfPasses;

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public int getNoOfPasses() {
		return noOfPasses;
	}

	public void setNoOfPasses(int noOfPasses) {
		this.noOfPasses = noOfPasses;
	}

	@ManyToOne
	@JoinColumn(name = "event_type_id", referencedColumnName = "id", nullable = false)
	private EventTypes eventTypes;

	public EventTypes getEventTypes() {
		return eventTypes;
	}

	public void setEventTypes(EventTypes eventTypes) {
		this.eventTypes = eventTypes;
	}

	public static Finder<Long, Events> find = new Finder<Long, Events>(Long.class, Events.class);

	public static void submit(Events events) {
		// TODO Auto-generated method stub
		events.saveModel();
	}

	public static List<Events> all() {
		// TODO Auto-generated method stub
		return find.all();
	}

}
