package models.masters;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import models.BaseModel;
import models.transactions.UserOffers;

@Entity
@Table(name = "Offers")
public class Offers extends BaseModel {

	/** System generated Serial Version UID. */
	private static final long serialVersionUID = 2077692996793963617L;

	private String name;

	private String description;

	private String timeLeft;

	private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTimeLeft() {
		return timeLeft;
	}

	public void setTimeLeft(String timeLeft) {
		this.timeLeft = timeLeft;
	}

	@OneToMany(mappedBy = "offers", cascade = CascadeType.ALL)
	private List<UserOffers> userOffers;

	public List<UserOffers> getUserOffers() {
		return userOffers;
	}

	public void setUserOffers(List<UserOffers> userOffers) {
		this.userOffers = userOffers;
	}

	public static void submit(Offers offers) {
		// TODO Auto-generated method stub
		offers.saveModel();
	}

	public static Finder<Long, Offers> find = new Finder<Long, Offers>(Long.class, Offers.class);

	public static List<Offers> getOffersByType(String type) {
		return find.where().eq("type", type).findList();

	}

	public static List<Offers> findAll() {
		// TODO Auto-generated method stub
		return find.all();
	}

}
