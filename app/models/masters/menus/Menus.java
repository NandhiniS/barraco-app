package models.masters.menus;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import models.BaseModel;

import com.utils.ApplicationUtils;

@Entity
@Table(name = "menus")
public class Menus extends BaseModel {

	/** System generated Serial Version UID. */
	private static final long serialVersionUID = -8402022814430837269L;

	private String menuCode;

	private String menuName;

	private String subMenuCode;

	private String subMenuName;

	private String itemCode;

	private String itemName;

	private String itemDescription;

	private BigDecimal itemPrice;

	private BigDecimal itemHappyHourPrice;

	private String itemType;

	private String itemImageUrl;

	private BigDecimal itemPotentialPrice;

	private String itemAccount;

	@OneToMany(mappedBy = "menu")
	private List<AddOnItems> addOnItems;

	@OneToMany(mappedBy = "menu")
	private List<MenuItemClassifier> menuItemClassfiers;

	public String getMenuCode() {
		return menuCode;
	}

	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getSubMenuCode() {
		return subMenuCode;
	}

	public void setSubMenuCode(String subMenuCode) {
		this.subMenuCode = subMenuCode;
	}

	public String getSubMenuName() {
		return subMenuName;
	}

	public void setSubMenuName(String subMenuName) {
		this.subMenuName = subMenuName;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemDescription() {
		return itemDescription;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}

	public BigDecimal getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(BigDecimal itemPrice) {
		this.itemPrice = itemPrice;
	}

	public BigDecimal getItemHappyHourPrice() {
		return itemHappyHourPrice;
	}

	public void setItemHappyHourPrice(BigDecimal itemHappyHourPrice) {
		this.itemHappyHourPrice = itemHappyHourPrice;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getItemImageUrl() {
		return itemImageUrl;
	}

	public void setItemImageUrl(String itemImageUrl) {
		this.itemImageUrl = itemImageUrl;
	}

	public BigDecimal getItemPotentialPrice() {
		return itemPotentialPrice;
	}

	public void setItemPotentialPrice(BigDecimal itemPotentialPrice) {
		this.itemPotentialPrice = itemPotentialPrice;
	}

	public String getItemAccount() {
		return itemAccount;
	}

	public void setItemAccount(String itemAccount) {
		this.itemAccount = itemAccount;
	}

	public List<AddOnItems> getAddOnItems() {
		return addOnItems;
	}

	public void setAddOnItems(List<AddOnItems> addOnItems) {
		this.addOnItems = addOnItems;
	}

	public static final Finder<Long, Menus> finder = new Finder<Long, Menus>(Long.class, Menus.class);

	public static Menus findById(Long id) {
		Menus menu = finder.byId(id);
		return menu;
	}

	public static List<Menus> findBySubMenuCode(String key) {
		List<Menus> menuList = finder.where().eq("subMenuCode", key).findList();
		return menuList;
	}

	public static List<Menus> findByMenuCode(String key) {
		List<Menus> menuList = finder.where().eq("menuCode", key).findList();
		return menuList;
	}

	public static Menus findByItemCode(String key) {
		Menus menu = finder.where().eq("itemCode", key).findUnique();
		return menu;
	}

	public String getProductFilePath() {
		// TODO: Remove commented code if the file path work correctly.
		// return "public/schedules/" + this.getUuid() + "-policy.pdf";
		return ApplicationUtils.getProductsFilePath(this.subMenuCode) + "/" + this.itemImageUrl;
	}

	public List<MenuItemClassifier> getMenuItemClassfiers() {
		return menuItemClassfiers;
	}

	public void setMenuItemClassfiers(List<MenuItemClassifier> menuItemClassfiers) {
		this.menuItemClassfiers = menuItemClassfiers;
	}

}
