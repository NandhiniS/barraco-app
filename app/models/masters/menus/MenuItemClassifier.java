package models.masters.menus;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.BaseModel;

@Entity
@Table(name = "menu_item_classifiers")
public class MenuItemClassifier extends BaseModel {

	/** System generated Serial Version UID. */
	private static final long serialVersionUID = 404063997775596127L;

	@ManyToOne
	@JoinColumn(name = "menu_id", referencedColumnName = "id")
	private Menus menu;

	private String classifierCode;

	private String classifierName;

	private String classifierImageUrl;

	public Menus getMenu() {
		return menu;
	}

	public void setMenu(Menus menu) {
		this.menu = menu;
	}

	public String getClassifierCode() {
		return classifierCode;
	}

	public void setClassifierCode(String classifierCode) {
		this.classifierCode = classifierCode;
	}

	public String getClassifierName() {
		return classifierName;
	}

	public void setClassifierName(String classifierName) {
		this.classifierName = classifierName;
	}

	public String getClassifierImageUrl() {
		return classifierImageUrl;
	}

	public void setClassifierImageUrl(String classifierImageUrl) {
		this.classifierImageUrl = classifierImageUrl;
	}

}
