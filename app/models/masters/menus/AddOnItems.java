package models.masters.menus;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.BaseModel;

@Entity
@Table(name = "add_on_items")
public class AddOnItems extends BaseModel {

	/** System generated Serial Version UID. */
	private static final long serialVersionUID = -7143346692588340018L;

	@ManyToOne
	@JoinColumn(name = "menu_id", referencedColumnName = "id")
	private Menus menu;

	private String addOnCode;

	private String addOnName;

	private String addOnType;

	private BigDecimal addOnPrice;

	private BigDecimal addOnHappyHourPrice;

	private BigDecimal addOnPotentialPrice;

	private String addOnAccount;

	public Menus getMenu() {
		return menu;
	}

	public void setMenu(Menus menu) {
		this.menu = menu;
	}

	public String getAddOnCode() {
		return addOnCode;
	}

	public void setAddOnCode(String addOnCode) {
		this.addOnCode = addOnCode;
	}

	public String getAddOnName() {
		return addOnName;
	}

	public void setAddOnName(String addOnName) {
		this.addOnName = addOnName;
	}

	public String getAddOnType() {
		return addOnType;
	}

	public void setAddOnType(String addOnType) {
		this.addOnType = addOnType;
	}

	public BigDecimal getAddOnPrice() {
		return addOnPrice;
	}

	public void setAddOnPrice(BigDecimal addOnPrice) {
		this.addOnPrice = addOnPrice;
	}

	public BigDecimal getAddOnHappyHourPrice() {
		return addOnHappyHourPrice;
	}

	public void setAddOnHappyHourPrice(BigDecimal addOnHappyHourPrice) {
		this.addOnHappyHourPrice = addOnHappyHourPrice;
	}

	public BigDecimal getAddOnPotentialPrice() {
		return addOnPotentialPrice;
	}

	public void setAddOnPotentialPrice(BigDecimal addOnPotentialPrice) {
		this.addOnPotentialPrice = addOnPotentialPrice;
	}

	public String getAddOnAccount() {
		return addOnAccount;
	}

	public void setAddOnAccount(String addOnAccount) {
		this.addOnAccount = addOnAccount;
	}

}
