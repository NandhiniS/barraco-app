package models.masters;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import models.BaseModel;
import models.transactions.UserRegistration;

/**
 * @author nandhini
 * 
 */

@Entity
@Table(name = "role")
public class Role extends BaseModel {

	/** System generated Serial Version UID. */
	private static final long serialVersionUID = -5687740048772245442L;
	private String role;

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@OneToMany(mappedBy = "role", cascade = CascadeType.ALL)
	UserRegistration user;

	public UserRegistration getUser() {
		return user;
	}

	public void setUser(UserRegistration user) {
		this.user = user;
	}

	public static Finder<Long, Role> finder = new Finder<Long, Role>(Long.class, Role.class);

	public static List<Role> all() {
		// TODO Auto-generated method stub
		return finder.all();
	}

	public static Role findById(Long id) {
		Role role = finder.byId(id);
		return role;
	}

	public static Role findByRoleName(String name) {
		Role role = finder.where().eq("role", name).findUnique();
		return role;
	}

}
