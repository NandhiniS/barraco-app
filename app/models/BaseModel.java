package models;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import play.db.ebean.Model;

@MappedSuperclass
public class BaseModel extends Model {

	/** System generated Serial Version UID. */
	private static final long serialVersionUID = 4593437573834836875L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(nullable = false)
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	// Restrict direct usage - Add other methods too later
	@Override
	public void save() {
		throw new UnsupportedOperationException("Direct usage of save() restricted, use saveModel instead.");
	}

	@Override
	public void update() {
		throw new UnsupportedOperationException("Direct usage of update() restricted, use updateModel instead.");
	}

	// Convenience Methods

	public Boolean saveModel() {
		super.save();
		return Boolean.TRUE;
	}

	public Boolean updateModel() {
		super.update();
		this.refresh();
		return Boolean.TRUE;
	}

	public void removeModel() {
		if (this.getId() > 0) {
			this.updateModel();
		} else {
			this.saveModel();
		}
	}
}
