package models.transactions;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.BaseModel;
import models.masters.Events;

@Entity
@Table(name = "passpurchases")
public class PassPurchases extends BaseModel {

	/** System generated Serial Version UID. */
	private static final long serialVersionUID = -8135057018382696567L;
	private int tableNo;

	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
	private UserRegistration userRegister;

	@ManyToOne
	@JoinColumn(name = "event_id", referencedColumnName = "id", nullable = false)
	private Events events;
	private String attenderName;
	private String passType;
	private int no_of_passes;

	public String getPassType() {
		return passType;
	}

	public void setPassType(String passType) {
		this.passType = passType;
	}

	public int getNo_of_passes() {
		return no_of_passes;
	}

	public void setNo_of_passes(int no_of_passes) {
		this.no_of_passes = no_of_passes;
	}

	public Events getEvents() {
		return events;
	}

	public void setEvents(Events events) {
		this.events = events;
	}

	public int getTableNo() {
		return tableNo;
	}

	public void setTableNo(int tableNo) {
		this.tableNo = tableNo;
	}

	public String getAttenderName() {
		return attenderName;
	}

	public void setAttenderName(String attenderName) {
		this.attenderName = attenderName;
	}

	public UserRegistration getUserRegister() {
		return userRegister;
	}

	public void setUserRegister(UserRegistration userRegister) {
		this.userRegister = userRegister;
	}

	public static Finder<Long, PassPurchases> find = new Finder<Long, PassPurchases>(Long.class, PassPurchases.class);

	public static List<PassPurchases> all() {
		// TODO Auto-generated method stub
		return find.all();
	}

}
