package models.transactions;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.BaseModel;
import models.masters.menus.Menus;

@Entity
@Table(name = "orders")
public class Orders extends BaseModel {

	/** System generated Serial Version UID. */
	private static final long serialVersionUID = 4125091284990248792L;

	@ManyToOne
	@JoinColumn(name = "user_order_id", referencedColumnName = "id", nullable = false)
	private UserOrders userOrder;

	@ManyToOne
	@JoinColumn(name = "menu_id", referencedColumnName = "id", nullable = false)
	private Menus menu;

	private Integer quantity;

	private String quantityType;

	private BigDecimal itemPrice;

	private BigDecimal itemTax;

	private BigDecimal itemTotalPrice;

	public UserOrders getUserOrder() {
		return userOrder;
	}

	public void setUserOrder(UserOrders userOrder) {
		this.userOrder = userOrder;
	}

	public Menus getMenu() {
		return menu;
	}

	public void setMenu(Menus menu) {
		this.menu = menu;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(BigDecimal itemPrice) {
		this.itemPrice = itemPrice;
	}

	public BigDecimal getItemTax() {
		return itemTax;
	}

	public void setItemTax(BigDecimal itemTax) {
		this.itemTax = itemTax;
	}

	public BigDecimal getItemTotalPrice() {
		return itemTotalPrice;
	}

	public void setItemTotalPrice(BigDecimal itemTotalPrice) {
		this.itemTotalPrice = itemTotalPrice;
	}

	public String getQuantityType() {
		return quantityType;
	}

	public void setQuantityType(String quantityType) {
		this.quantityType = quantityType;
	}

	public static Finder<Long, Orders> finder = new Finder<Long, Orders>(Long.class, Orders.class);

	public static List<Orders> findAll() {
		return finder.all();
	}

	public static List<Orders> findByUserOrderId(Long userOrderId) {
		List<Orders> orderList = finder.where().eq("userOrder.id", userOrderId).findList();
		return orderList;
	}

}
