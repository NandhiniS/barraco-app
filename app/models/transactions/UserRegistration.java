package models.transactions;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import models.BaseModel;
import models.masters.Role;

/**
 * @author nandhini
 * 
 */
@Entity
@Table(name = "userregistration")
public class UserRegistration extends BaseModel {

	/** System generated Serial Version UID. */
	private static final long serialVersionUID = 4169151094983597359L;
	private String userName;
	private String password;
	private String sessionId;
	private Timestamp createdAt;
	private Timestamp statusAt;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	private List<UserOffers> userOffers;

	public List<UserOffers> getUserOffers() {
		return userOffers;
	}

	public void setUserOffers(List<UserOffers> userOffers) {
		this.userOffers = userOffers;
	}

	@ManyToOne
	@JoinColumn(name = "role_id", referencedColumnName = "id", nullable = false)
	private Role role;

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public static Finder<Long, UserRegistration> finder = new Finder<Long, UserRegistration>(Long.class, UserRegistration.class);

	public static List<UserRegistration> all() {
		// TODO Auto-generated method stub
		return finder.all();
	}

	public static UserRegistration findById(Long id) {
		return finder.byId(id);
	}

	public static UserRegistration findByIdAndSessionId(Long id, String sessionId) {
		UserRegistration user = finder.where().eq("id", id).eq("sessionId", sessionId).findUnique();
		return user;
	}

	public static UserRegistration findByName(String name) {
		UserRegistration user = finder.where().eq("userName", name).findUnique();
		return user;
	}

	public static UserRegistration authorize(UserRegistration user) {
		UserRegistration loginUser = finder.where().eq("userName", user.getUserName()).eq("password", user.getPassword()).findUnique();
		return loginUser;
	}

	public UserRegistration submit() {
		this.setStatusAt(new Timestamp(System.currentTimeMillis()));
		this.setCreatedAt(new Timestamp(System.currentTimeMillis()));
		this.setSessionId(UUID.randomUUID().toString().replaceAll("-", ""));
		this.saveModel();
		return this;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public Timestamp getStatusAt() {
		return statusAt;
	}

	public void setStatusAt(Timestamp statusAt) {
		this.statusAt = statusAt;
	}

}
