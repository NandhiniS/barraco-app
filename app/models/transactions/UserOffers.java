package models.transactions;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.BaseModel;
import models.masters.Offers;

@Entity
@Table(name = "useroffers")
public class UserOffers extends BaseModel {

	/** System generated Serial Version UID. */
	private static final long serialVersionUID = -1958642778312317334L;

	@ManyToOne
	@JoinColumn(name = "offer_id", referencedColumnName = "id", nullable = false)
	private Offers offers;
	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
	private UserRegistration user;

	public UserRegistration getUser() {
		return user;
	}

	public void setUser(UserRegistration user) {
		this.user = user;
	}

	public Offers getOffers() {
		return offers;
	}

	public void setOffers(Offers offers) {
		this.offers = offers;
	}

	public static Finder<Long, UserOffers> find = new Finder<Long, UserOffers>(Long.class, UserOffers.class);

	public static List<UserOffers> all() {
		// TODO Auto-generated method stub
		return find.all();
	}

}
