package models.transactions;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import models.BaseModel;
import models.masters.Events;
import models.masters.Offers;

@Entity
@Table(name = "user_orders")
public class UserOrders extends BaseModel {

	/** System generated Serial Version UID. */
	private static final long serialVersionUID = 8520146012242407095L;

	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
	private UserRegistration user;

	@OneToMany(mappedBy = "userOrder")
	private List<Orders> orders;

	@ManyToOne
	@JoinColumn(name = "offer_id", referencedColumnName = "id", nullable = false)
	private Offers offers;

	@ManyToOne
	@JoinColumn(name = "event_id", referencedColumnName = "id", nullable = false)
	private Events events;

	private String sessionId;
	private Timestamp createdAt;
	private Timestamp statusAt;

	public UserRegistration getUser() {
		return user;
	}

	public void setUser(UserRegistration user) {
		this.user = user;
	}

	public Offers getOffers() {
		return offers;
	}

	public void setOffers(Offers offers) {
		this.offers = offers;
	}

	public Events getEvents() {
		return events;
	}

	public void setEvents(Events events) {
		this.events = events;
	}

	public static Finder<Long, UserOrders> finder = new Finder<Long, UserOrders>(Long.class, UserOrders.class);

	public static List<UserOrders> findAll() {
		return finder.all();
	}

	public static UserOrders findById(Long id) {
		UserOrders userOrder = finder.byId(id);
		return userOrder;
	}

	public static UserOrders findByUserAndSession(Long userId, String sessionId) {
		UserOrders userOrder = finder.where().eq("user.id", userId).eq("sessionId", sessionId).findUnique();
		return userOrder;
	}

	public static List<UserOrders> findByUser(Long userId) {
		List<UserOrders> userOrder = finder.where().eq("user.id", userId).findList();
		return userOrder;
	}

	public List<Orders> getOrders() {
		return orders;
	}

	public void setOrders(List<Orders> orders) {
		this.orders = orders;
	}

	public void addOrders(List<Orders> orders) {
		getOrders().addAll(orders);
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public Timestamp getStatusAt() {
		return statusAt;
	}

	public void setStatusAt(Timestamp statusAt) {
		this.statusAt = statusAt;
	}

}
