package com.utils;

import java.io.IOException;
import java.util.List;

import models.masters.menus.Menus;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.api.touche.response.Menu;
import com.api.touche.response.Menu.OutletMenus;
import com.api.touche.response.Menu.OutletMenus.SubMenus;
import com.api.touche.response.Menu.OutletMenus.SubMenus.MenuItems;
import com.api.touche.response.Menu.OutletMenus.SubMenus.MenuItems.AddOnItems;
import com.mapper.AddOnMapper;
import com.mapper.MenuMapper;

public class JsonUtils {

	private static Logger logger = LoggerFactory.getLogger(JsonUtils.class);

	public static void main(String args[]) throws JsonParseException, JsonMappingException, IOException {

		String json = "{ \"ResponseStatus\":{\"ResponseCode\":\"0\",\"ResponseFlag\":\"SUCCESS\",\"ResponseMessage\":\"test\"}" + "}";
		json = "{\"OutletConfiguration\":{\"OutletKeys\":\"NRNNNNNNYNNNNYNNNN\",\"HappyHours\":[],\"DefaultTable\":\"\",\"DefaultCover\":0,\"DepartmentCode\":\"RE\",\"DefaultServerId\":\"\",\"TaxVatDefinitions\":[{\"ChargeCode\":\"REBV\",\"TaxVatIndicator\":\"T\",\"TaxPercentage\":4.94,\"DiscountTax\":\"N\"},{\"ChargeCode\":\"REBV\",\"TaxVatIndicator\":\"V\",\"TaxPercentage\":14.5,\"DiscountTax\":\"N\"},"
				+ "{\"ChargeCode\":\"REBV\",\"TaxVatIndicator\":\"T\",\"TaxPercentage\":5,\"DiscountTax\":\"N\"},{\"ChargeCode\":\"REFD\",\"TaxVatIndicator\":\"T\",\"TaxPercentage\":5,\"DiscountTax\":\"N\"},{\"ChargeCode\":\"REFD\",\"TaxVatIndicator\":\"V\",\"TaxPercentage\":12.36,\"DiscountTax\":\"N\"},{\"ChargeCode\":\"REFD\",\"TaxVatIndicator\":\"T\",\"TaxPercentage\":4.94,\"DiscountTax\":\"N\"}]},"
				+ "\"OutletMenus\":[{\"MenuCode\":\"DEMO\",\"MenuName\":\"DEMO MENU\",\"SubMenus\":[{\"SubMenuCode\":\"DEMO\",\"SubMenuName\":\"DEMO MENU\",\"MenuItems\":"
				+ "[{\"ItemCode\":\"0074\",\"ItemName\":\"DARJEELING TEA\",\"ItemDescription\":null,\"ItemPrice\":100,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":100,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"BV\","
				+ "\"AddOnItems\":[{\"AddOnCode\": \"Z090\",\"AddOnName\": \"GARLIC NAAN\",\"AddOnType\": \"MO\",\"AddOnPrice\": 0,\"AddOnHappyHourPrice\":0,\"AddOnPotentialPrice\":0,\"AddOnAccount\":\"FD\"}],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]},"
				+ "{\"ItemCode\":\"0075\",\"ItemName\":\"ASSAM TEA\",\"ItemDescription\":null,\"ItemPrice\":100,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":100,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"BV\",\"AddOnItems\":[],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]},{\"ItemCode\":\"0076\",\"ItemName\":\"JASMINE TEA\",\"ItemDescription\":null,\"ItemPrice\":100,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":100,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"BV\",\"AddOnItems\":[],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]},{\"ItemCode\":\"0077\",\"ItemName\":\"EARL GRAY TEA\",\"ItemDescription\":null,\"ItemPrice\":100,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":100,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"BV\",\"AddOnItems\":[],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]},{\"ItemCode\":\"0078\",\"ItemName\":\"LEMON TEA\",\"ItemDescription\":null,\"ItemPrice\":100,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":100,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"BV\",\"AddOnItems\":[],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]},{\"ItemCode\":\"0079\",\"ItemName\":\"ENGLISH BREAKFAST\",\"ItemDescription\":null,\"ItemPrice\":100,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":100,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"BV\",\"AddOnItems\":[],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]},{\"ItemCode\":\"0080\",\"ItemName\":\"PEPPER MINT\",\"ItemDescription\":null,\"ItemPrice\":100,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":100,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"BV\",\"AddOnItems\":[],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]},{\"ItemCode\":\"0081\",\"ItemName\":\"REGULAR COFFEE\",\"ItemDescription\":null,\"ItemPrice\":100,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":100,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"BV\",\"AddOnItems\":[],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]},{\"ItemCode\":\"0082\",\"ItemName\":\"FRESH LIME\",\"ItemDescription\":null,\"ItemPrice\":90,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":90,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"BV\",\"AddOnItems\":[],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]},{\"ItemCode\":\"0083\",\"ItemName\":\"LEMON ICE TEA\",\"ItemDescription\":null,\"ItemPrice\":90,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":90,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"BV\",\"AddOnItems\":[],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]},{\"ItemCode\":\"0084\",\"ItemName\":\"AERATED DRINK\",\"ItemDescription\":null,\"ItemPrice\":90,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":90,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"BV\",\"AddOnItems\":[],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]},{\"ItemCode\":\"0085\",\"ItemName\":\"CANNED JUICE\",\"ItemDescription\":null,\"ItemPrice\":90,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":90,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"BV\",\"AddOnItems\":[],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]},{\"ItemCode\":\"0086\",\"ItemName\":\"DIET COKE\",\"ItemDescription\":null,\"ItemPrice\":95,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":95,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"BV\",\"AddOnItems\":[],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]},{\"ItemCode\":\"0087\",\"ItemName\":\"MILK SHAKE\",\"ItemDescription\":null,\"ItemPrice\":165,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":165,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"BV\",\"AddOnItems\":[],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]},{\"ItemCode\":\"0088\",\"ItemName\":\"COLD COFFE WITH ICE CREAM\",\"ItemDescription\":null,\"ItemPrice\":165,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":165,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"BV\",\"AddOnItems\":[],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]},{\"ItemCode\":\"0089\",\"ItemName\":\"PACKAGE DRINKING WATER\",\"ItemDescription\":null,\"ItemPrice\":60,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":60,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"BV\",\"AddOnItems\":[],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]},{\"ItemCode\":\"0101\",\"ItemName\":\"STRAWBERRY SHAKE\",\"ItemDescription\":null,\"ItemPrice\":160,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":160,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"BV\",\"AddOnItems\":[],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]},{\"ItemCode\":\"0102\",\"ItemName\":\"CHOCOLATE SHAKE\",\"ItemDescription\":null,\"ItemPrice\":160,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":160,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"BV\",\"AddOnItems\":[],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]},{\"ItemCode\":\"0103\",\"ItemName\":\"VANILLA SHAKE\",\"ItemDescription\":null,\"ItemPrice\":160,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":160,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"BV\",\"AddOnItems\":[],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]},{\"ItemCode\":\"0105\",\"ItemName\":\"MISCELLANEOUS\",\"ItemDescription\":null,\"ItemPrice\":160,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":160,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"BV\",\"AddOnItems\":[],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]},{\"ItemCode\":\"0108\",\"ItemName\":\"RED BULL\",\"ItemDescription\":null,\"ItemPrice\":175,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":175,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"BV\",\"AddOnItems\":[],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]},{\"ItemCode\":\"0109\",\"ItemName\":\"SPRING CALAMARI\",\"ItemDescription\":null,\"ItemPrice\":200,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":200,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"FD\",\"AddOnItems\":[],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]},{\"ItemCode\":\"110\",\"ItemName\":\"CHICKEN CAPRESE\",\"ItemDescription\":null,\"ItemPrice\":295,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":0,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"FD\",\"AddOnItems\":[],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]},{\"ItemCode\":\"112\",\"ItemName\":\"BACON WR PRAWNS\",\"ItemDescription\":null,\"ItemPrice\":300,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":0,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"FD\",\"AddOnItems\":[],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]},{\"ItemCode\":\"114\",\"ItemName\":\"CEASAR SALAD\",\"ItemDescription\":null,\"ItemPrice\":300,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":0,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"FD\",\"AddOnItems\":[],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]},{\"ItemCode\":\"119\",\"ItemName\":\"PENNE CARBONERA\",\"ItemDescription\":null,\"ItemPrice\":350,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":0,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"FD\",\"AddOnItems\":[],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]},{\"ItemCode\":\"122\",\"ItemName\":\"PIZZA PLAZA\",\"ItemDescription\":null,\"ItemPrice\":750,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":0,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"FD\",\"AddOnItems\":[],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]},{\"ItemCode\":\"123\",\"ItemName\":\"VEAL MEDALION\",\"ItemDescription\":null,\"ItemPrice\":750,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":0,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"FD\",\"AddOnItems\":[],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]},{\"ItemCode\":\"124\",\"ItemName\":\"PRAWN IN WHITE WINE\",\"ItemDescription\":null,\"ItemPrice\":800,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":0,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"FD\",\"AddOnItems\":[],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]},{\"ItemCode\":\"126\",\"ItemName\":\"GRILLED LAMB RACK\",\"ItemDescription\":null,\"ItemPrice\":1200,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":0,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"FD\",\"AddOnItems\":[],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]},{\"ItemCode\":\"127\",\"ItemName\":\"SPRING MANICOTTI PARMAGINA.\",\"ItemDescription\":null,\"ItemPrice\":500,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":0,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"FD\",\"AddOnItems\":[],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]},{\"ItemCode\":\"135\",\"ItemName\":\"GREY GOOSE\",\"ItemDescription\":null,\"ItemPrice\":500,\"SubMenuCode\":\"DEMO\",\"ItemHappyHourPrice\":0,\"ItemType\":\"MI\",\"ItemImageUrl\":\"\",\"ItemPotentialPrice\":0,\"ItemAccount\":\"AB\",\"AddOnItems\":[],\"MenuItemClassifiers\":[],\"PackageConstituents\":[]}]}]}],\"OutletTables\":[],\"OutletItemClassifiers\":[],\"ResponseStatus\":{\"ResponseCode\":\"0\",\"ResponseFlag\":\"SUCCESS\",\"ResponseMessage\":\"\"}}";
		// converting JSON String to Java object
		try {

			Menu fromJson = bindFromJSON(json);
			List<OutletMenus> outletMenus = fromJson.getOutletMenus();
			for (OutletMenus eachOutletMenu : outletMenus) {
				List<SubMenus> subMenus = eachOutletMenu.getSubMenus();
				for (SubMenus eachSubMenu : subMenus) {
					List<MenuItems> menuItems = eachSubMenu.getMenuItems();
					for (MenuItems eachMenuItem : menuItems) {
						ModelMapper menuMap = new ModelMapper();
						menuMap.addMappings(new MenuMapper());
						Menus eachMenu = menuMap.map(eachMenuItem, Menus.class);
						eachMenu.setMenuCode(eachOutletMenu.getMenuCode());
						eachMenu.setMenuName(eachOutletMenu.getMenuName());
						eachMenu.setSubMenuCode(eachSubMenu.getSubMenuCode());
						eachMenu.setSubMenuName(eachSubMenu.getSubMenuName());
						System.out.println(eachMenu.getItemName());

						List<AddOnItems> addOnItems = eachMenuItem.getAddOnItems();
						for (AddOnItems eachAddOn : addOnItems) {
							ModelMapper addOnMap = new ModelMapper();
							addOnMap.addMappings(new AddOnMapper());
							models.masters.menus.AddOnItems addOns = addOnMap.map(eachAddOn, models.masters.menus.AddOnItems.class);
							addOns.setMenu(eachMenu);
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Menu bindFromJSON(String json) throws JsonParseException, JsonMappingException, IOException {
		Menu garima = new ObjectMapper().readValue(json, Menu.class);
		logger.info("Java Object created from JSON String ");
		logger.info("JSON String : " + json);
		logger.info("Java Object : " + garima);

		return garima;
	}

}
