package com.utils;

import play.Play;

public class ApplicationUtils {

	public static String getStaticFileBasePath() {
		return Play.application().configuration().getString("staticfilepath");
	}

	public static String getProductsFilePath(String product) {
		return getDrinksFilePath() + "/products/" + product;
	}

	public static String getDrinksFilePath() {
		return getStaticFileBasePath() + "drinks" ;
	}
}
