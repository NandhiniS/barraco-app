package com.mapper;

import models.masters.menus.Menus;

import org.modelmapper.PropertyMap;

import com.api.touche.response.Menu.OutletMenus.SubMenus.MenuItems;

public class MenuMapper extends PropertyMap<MenuItems, Menus> {

	@Override
	protected void configure() {
		map().setItemCode(source.getItemCode());
		map().setItemName(source.getItemName());
		map().setItemDescription(source.getItemDescription());
		map().setItemPrice(source.getItemPrice());
		map().setItemHappyHourPrice(source.getItemHappyHourPrice());
		map().setItemType(source.getItemType());
		map().setItemImageUrl(source.getItemImageUrl());
		map().setItemPotentialPrice(source.getItemPotentialPrice());
		map().setItemAccount(source.getItemAccount());

	}

}
