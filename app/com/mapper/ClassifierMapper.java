package com.mapper;

import models.masters.menus.MenuItemClassifier;

import org.modelmapper.PropertyMap;

public class ClassifierMapper extends PropertyMap<com.api.touche.response.Menu.OutletMenus.SubMenus.MenuItems.MenuItemClassifiers, MenuItemClassifier> {

	@Override
	protected void configure() {

		map().setClassifierCode(source.getClassifierCode());
		map().setClassifierName(source.getClassifierName());
		map().setClassifierImageUrl(source.getClassifierImageUrl());
	}

}
