package com.mapper;

import models.masters.menus.AddOnItems;

import org.modelmapper.PropertyMap;

public class AddOnMapper extends PropertyMap<com.api.touche.response.Menu.OutletMenus.SubMenus.MenuItems.AddOnItems, AddOnItems> {

	@Override
	protected void configure() {

		map().setAddOnCode(source.getAddOnCode());
		map().setAddOnName(source.getAddOnName());
		map().setAddOnType(source.getAddOnType());
		map().setAddOnPrice(source.getAddOnPrice());
		map().setAddOnHappyHourPrice(source.getAddOnHappyHourPrice());
		map().setAddOnPotentialPrice(source.getAddOnPotentialPrice());
		map().setAddOnAccount(source.getAddOnAccount());
	}

}
