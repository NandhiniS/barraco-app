package com.api.tabletop.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import models.masters.menus.AddOnItems;
import models.masters.menus.MenuItemClassifier;
import models.masters.menus.Menus;

public class MenusDTO {

	private List<MenuResponse> menuList = new ArrayList<MenuResponse>();

	public MenusDTO(List<Menus> menus) {
		for (Menus menu : menus) {
			MenuResponse menuRes = new MenuResponse(menu);
			this.menuList.add(menuRes);
		}

	}

	public MenusDTO(Menus menu) {
		MenuResponse menuRes = new MenuResponse(menu);
		this.menuList.add(menuRes);
	}

	public static class MenuResponse {

		private Long id;

		private String menuCode;

		private String menuName;

		private String subMenuCode;

		private String subMenuName;

		private String itemCode;

		private String itemName;

		private String itemDescription;

		private BigDecimal itemPrice;

		private BigDecimal itemHappyHourPrice;

		private String itemType;

		private String itemImageUrl;

		private BigDecimal itemPotentialPrice;

		private String itemAccount;

		private List<AddOnResponse> addOnItems = new ArrayList<MenusDTO.MenuResponse.AddOnResponse>();

		private List<MenuClassifierResponse> menuItemClassfiers = new ArrayList<MenusDTO.MenuResponse.MenuClassifierResponse>();

		public MenuResponse(Menus menu) {

			this.id = menu.getId();
			this.menuCode = menu.getMenuCode();
			this.menuName = menu.getMenuCode();
			this.subMenuCode = menu.getSubMenuCode();
			this.subMenuName = menu.getSubMenuName();
			this.itemCode = menu.getItemCode();
			this.itemName = menu.getItemName();
			this.itemDescription = menu.getItemDescription();
			this.itemPrice = menu.getItemPrice();
			this.itemHappyHourPrice = menu.getItemHappyHourPrice();
			this.itemType = menu.getItemType();
			this.itemImageUrl = menu.getItemImageUrl();
			this.itemPotentialPrice = menu.getItemPotentialPrice();
			this.itemAccount = menu.getItemAccount();

			List<MenuItemClassifier> menuItemClassfiers = menu.getMenuItemClassfiers();
			for (MenuItemClassifier menuClassifier : menuItemClassfiers) {
				MenuClassifierResponse classRes = new MenuClassifierResponse(menuClassifier);
				this.menuItemClassfiers.add(classRes);
			}

			List<AddOnItems> addOnItems2 = menu.getAddOnItems();
			for (AddOnItems addOns : addOnItems2) {
				AddOnResponse addOnRes = new AddOnResponse(addOns);
				this.addOnItems.add(addOnRes);
			}
		}

		public static class MenuClassifierResponse {

			private Long id;

			private String classifierCode;

			private String classifierName;

			private String classifierImageUrl;

			public MenuClassifierResponse(MenuItemClassifier classifier) {

				this.id = classifier.getId();
				this.classifierCode = classifier.getClassifierCode();
				this.classifierName = classifier.getClassifierName();
				this.classifierImageUrl = classifier.getClassifierImageUrl();
			}

			public Long getId() {
				return id;
			}

			public void setId(Long id) {
				this.id = id;
			}

			public String getClassifierCode() {
				return classifierCode;
			}

			public void setClassifierCode(String classifierCode) {
				this.classifierCode = classifierCode;
			}

			public String getClassifierName() {
				return classifierName;
			}

			public void setClassifierName(String classifierName) {
				this.classifierName = classifierName;
			}

			public String getClassifierImageUrl() {
				return classifierImageUrl;
			}

			public void setClassifierImageUrl(String classifierImageUrl) {
				this.classifierImageUrl = classifierImageUrl;
			}
		}

		public static class AddOnResponse {

			private Long id;

			private String addOnCode;

			private String addOnName;

			private String addOnType;

			private BigDecimal addOnPrice;

			private BigDecimal addOnHappyHourPrice;

			private BigDecimal addOnPotentialPrice;

			private String addOnAccount;

			public AddOnResponse(AddOnItems addOns) {

				this.id = addOns.getId();
				this.addOnCode = addOns.getAddOnCode();
				this.addOnName = addOns.getAddOnName();
				this.addOnType = addOns.getAddOnType();
				this.addOnPrice = addOns.getAddOnPrice();
				this.addOnHappyHourPrice = addOns.getAddOnHappyHourPrice();
				this.addOnPotentialPrice = addOns.getAddOnPotentialPrice();
				this.addOnAccount = addOns.getAddOnAccount();
			}

			public Long getId() {
				return id;
			}

			public void setId(Long id) {
				this.id = id;
			}

			public String getAddOnCode() {
				return addOnCode;
			}

			public void setAddOnCode(String addOnCode) {
				this.addOnCode = addOnCode;
			}

			public String getAddOnName() {
				return addOnName;
			}

			public void setAddOnName(String addOnName) {
				this.addOnName = addOnName;
			}

			public String getAddOnType() {
				return addOnType;
			}

			public void setAddOnType(String addOnType) {
				this.addOnType = addOnType;
			}

			public BigDecimal getAddOnPrice() {
				return addOnPrice;
			}

			public void setAddOnPrice(BigDecimal addOnPrice) {
				this.addOnPrice = addOnPrice;
			}

			public BigDecimal getAddOnHappyHourPrice() {
				return addOnHappyHourPrice;
			}

			public void setAddOnHappyHourPrice(BigDecimal addOnHappyHourPrice) {
				this.addOnHappyHourPrice = addOnHappyHourPrice;
			}

			public BigDecimal getAddOnPotentialPrice() {
				return addOnPotentialPrice;
			}

			public void setAddOnPotentialPrice(BigDecimal addOnPotentialPrice) {
				this.addOnPotentialPrice = addOnPotentialPrice;
			}

			public String getAddOnAccount() {
				return addOnAccount;
			}

			public void setAddOnAccount(String addOnAccount) {
				this.addOnAccount = addOnAccount;
			}

		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getMenuCode() {
			return menuCode;
		}

		public void setMenuCode(String menuCode) {
			this.menuCode = menuCode;
		}

		public String getMenuName() {
			return menuName;
		}

		public void setMenuName(String menuName) {
			this.menuName = menuName;
		}

		public String getSubMenuCode() {
			return subMenuCode;
		}

		public void setSubMenuCode(String subMenuCode) {
			this.subMenuCode = subMenuCode;
		}

		public String getSubMenuName() {
			return subMenuName;
		}

		public void setSubMenuName(String subMenuName) {
			this.subMenuName = subMenuName;
		}

		public String getItemCode() {
			return itemCode;
		}

		public void setItemCode(String itemCode) {
			this.itemCode = itemCode;
		}

		public String getItemName() {
			return itemName;
		}

		public void setItemName(String itemName) {
			this.itemName = itemName;
		}

		public String getItemDescription() {
			return itemDescription;
		}

		public void setItemDescription(String itemDescription) {
			this.itemDescription = itemDescription;
		}

		public BigDecimal getItemPrice() {
			return itemPrice;
		}

		public void setItemPrice(BigDecimal itemPrice) {
			this.itemPrice = itemPrice;
		}

		public BigDecimal getItemHappyHourPrice() {
			return itemHappyHourPrice;
		}

		public void setItemHappyHourPrice(BigDecimal itemHappyHourPrice) {
			this.itemHappyHourPrice = itemHappyHourPrice;
		}

		public String getItemType() {
			return itemType;
		}

		public void setItemType(String itemType) {
			this.itemType = itemType;
		}

		public String getItemImageUrl() {
			return itemImageUrl;
		}

		public void setItemImageUrl(String itemImageUrl) {
			this.itemImageUrl = itemImageUrl;
		}

		public BigDecimal getItemPotentialPrice() {
			return itemPotentialPrice;
		}

		public void setItemPotentialPrice(BigDecimal itemPotentialPrice) {
			this.itemPotentialPrice = itemPotentialPrice;
		}

		public String getItemAccount() {
			return itemAccount;
		}

		public void setItemAccount(String itemAccount) {
			this.itemAccount = itemAccount;
		}

		// public List<AddOnItems> getAddOnItems() {
		// return addOnItems;
		// }
		//
		// public void setAddOnItems(List<AddOnItems> addOnItems) {
		// this.addOnItems = addOnItems;
		// }

		public List<MenuClassifierResponse> getMenuItemClassfiers() {
			return menuItemClassfiers;
		}

		public void setMenuItemClassfiers(List<MenuClassifierResponse> menuItemClassfiers) {
			this.menuItemClassfiers = menuItemClassfiers;
		}
	}

	public List<MenuResponse> getMenuList() {
		return menuList;
	}

	public void setMenuList(List<MenuResponse> menuList) {
		this.menuList = menuList;
	}
}
