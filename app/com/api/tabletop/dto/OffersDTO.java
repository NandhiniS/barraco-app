package com.api.tabletop.dto;

import java.util.ArrayList;
import java.util.List;

import models.masters.Offers;

public class OffersDTO {

	private List<OffersResponse> offerList = new ArrayList<OffersResponse>();

	public OffersDTO(List<Offers> offers) {
		for (Offers address : offers) {
			OffersResponse addDet = new OffersResponse(address);
			this.offerList.add(addDet);
		}

	}

	public static class OffersResponse {

		private Long id;

		private String name;

		private String description;

		private String timeLeft;

		private String type;

		public OffersResponse(Offers offer) {
			this.id = offer.getId();
			this.name = offer.getName();
			this.description = offer.getDescription();
			this.timeLeft = offer.getTimeLeft();
			this.type = offer.getType();
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public String getTimeLeft() {
			return timeLeft;
		}

		public void setTimeLeft(String timeLeft) {
			this.timeLeft = timeLeft;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}
	}

	public List<OffersResponse> getOfferList() {
		return offerList;
	}

	public void setOfferList(List<OffersResponse> offers) {
		this.offerList = offers;
	}
}
