package com.api.tabletop.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import models.transactions.Orders;

public class BillDTO {

	private List<BillResponse> billList = new ArrayList<BillResponse>();

	public BillDTO(List<Orders> menus) {
		for (Orders order : menus) {
			BillResponse billRes = new BillResponse(order);
			this.billList.add(billRes);
		}

	}

	public static class BillResponse {

		private String itemCategory;

		private String itemName;

		private String table;

		private String quantity;

		private BigDecimal itemPrice;

		public BillResponse(Orders order) {

			this.itemCategory = order.getMenu().getMenuCode();
			this.itemName = order.getMenu().getItemName();
			this.quantity = order.getQuantityType() + " - " + order.getQuantity();
			this.itemPrice = order.getItemTotalPrice();
		}

		public String getItemCategory() {
			return itemCategory;
		}

		public void setItemCategory(String itemCategory) {
			this.itemCategory = itemCategory;
		}

		public String getItemName() {
			return itemName;
		}

		public void setItemName(String itemName) {
			this.itemName = itemName;
		}

		public String getTable() {
			return table;
		}

		public void setTable(String table) {
			this.table = table;
		}

		public String getQuantity() {
			return quantity;
		}

		public void setQuantity(String quantity) {
			this.quantity = quantity;
		}

		public BigDecimal getItemPrice() {
			return itemPrice;
		}

		public void setItemPrice(BigDecimal itemPrice) {
			this.itemPrice = itemPrice;
		}
	}

	public List<BillResponse> getBillList() {
		return billList;
	}

	public void setBillList(List<BillResponse> billList) {
		this.billList = billList;
	}
}
