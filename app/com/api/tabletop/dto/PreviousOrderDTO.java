package com.api.tabletop.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import models.transactions.Orders;

public class PreviousOrderDTO {

	private List<OrderResponse> orderList = new ArrayList<OrderResponse>();

	public PreviousOrderDTO(List<Orders> orders) {
		for (Orders order : orders) {
			OrderResponse menuRes = new OrderResponse(order);
			this.orderList.add(menuRes);
		}

	}

	public static class OrderResponse {

		private Integer quantity;

		private String quantityType;

		private BigDecimal itemPrice;

		private BigDecimal itemTax;

		private BigDecimal itemTotalPrice;

		private MenusDTO menuResponse;

		public OrderResponse(Orders order) {
			this.quantity = order.getQuantity();
			this.quantityType = order.getQuantityType();
			this.itemPrice = order.getItemPrice();
			this.itemTax = order.getItemTax();
			this.itemTotalPrice = order.getItemTotalPrice();
			this.setMenuResponse(new MenusDTO(order.getMenu()));
		}

		public Integer getQuantity() {
			return quantity;
		}

		public void setQuantity(Integer quantity) {
			this.quantity = quantity;
		}

		public String getQuantityType() {
			return quantityType;
		}

		public void setQuantityType(String quantityType) {
			this.quantityType = quantityType;
		}

		public BigDecimal getItemPrice() {
			return itemPrice;
		}

		public void setItemPrice(BigDecimal itemPrice) {
			this.itemPrice = itemPrice;
		}

		public BigDecimal getItemTax() {
			return itemTax;
		}

		public void setItemTax(BigDecimal itemTax) {
			this.itemTax = itemTax;
		}

		public BigDecimal getItemTotalPrice() {
			return itemTotalPrice;
		}

		public void setItemTotalPrice(BigDecimal itemTotalPrice) {
			this.itemTotalPrice = itemTotalPrice;
		}

		public MenusDTO getMenuResponse() {
			return menuResponse;
		}

		public void setMenuResponse(MenusDTO menuResponse) {
			this.menuResponse = menuResponse;
		}

	}

	public List<OrderResponse> getOrderList() {
		return orderList;
	}

	public void setOrderList(List<OrderResponse> orderList) {
		this.orderList = orderList;
	}
}