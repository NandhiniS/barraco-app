package com.api.touche.response;

import java.math.BigDecimal;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

public class Menu {

	@JsonProperty("OutletConfiguration")
	private OutletConfiguration OutletConfiguration;

	public static class OutletConfiguration {

		@JsonProperty("OutletKeys")
		private String OutletKeys;

		@JsonProperty("ResponseFlag")
		private String responseFlag;

		@JsonProperty("DefaultTable")
		private String DefaultTable;

		@JsonProperty("DefaultCover")
		private Integer DefaultCover;

		@JsonProperty("DepartmentCode")
		private String DepartmentCode;

		@JsonProperty("DefaultServerId")
		private String DefaultServerId;

		@JsonProperty("HappyHours")
		private List<HappyHours> HappyHours;

		public static class HappyHours {

			@JsonProperty("StartTime")
			private String StartTime;

			@JsonProperty("EndTime")
			private String EndTime;

			public String getStartTime() {
				return StartTime;
			}

			public void setStartTime(String startTime) {
				StartTime = startTime;
			}

			public String getEndTime() {
				return EndTime;
			}

			public void setEndTime(String endTime) {
				EndTime = endTime;
			}

		}

		@JsonProperty("TaxVatDefinitions")
		private List<TaxVatDefinitions> TaxVatDefinitions;

		public static class TaxVatDefinitions {

			@JsonProperty("ChargeCode")
			private String ChargeCode;

			@JsonProperty("TaxVatIndicator")
			private String TaxVatIndicator;

			@JsonProperty("TaxPercentage")
			private BigDecimal TaxPercentage;

			@JsonProperty("DiscountTax")
			private String DiscountTax;

			public String getChargeCode() {
				return ChargeCode;
			}

			public void setChargeCode(String chargeCode) {
				ChargeCode = chargeCode;
			}

			public String getTaxVatIndicator() {
				return TaxVatIndicator;
			}

			public void setTaxVatIndicator(String taxVatIndicator) {
				TaxVatIndicator = taxVatIndicator;
			}

			public BigDecimal getTaxPercentage() {
				return TaxPercentage;
			}

			public void setTaxPercentage(BigDecimal taxPercentage) {
				TaxPercentage = taxPercentage;
			}

			public String getDiscountTax() {
				return DiscountTax;
			}

			public void setDiscountTax(String discountTax) {
				DiscountTax = discountTax;
			}

		}

		public String getOutletKeys() {
			return OutletKeys;
		}

		public void setOutletKeys(String outletKeys) {
			OutletKeys = outletKeys;
		}

		public String getResponseFlag() {
			return responseFlag;
		}

		public void setResponseFlag(String responseFlag) {
			this.responseFlag = responseFlag;
		}

		public String getDefaultTable() {
			return DefaultTable;
		}

		public void setDefaultTable(String defaultTable) {
			DefaultTable = defaultTable;
		}

		public Integer getDefaultCover() {
			return DefaultCover;
		}

		public void setDefaultCover(Integer defaultCover) {
			DefaultCover = defaultCover;
		}

		public String getDepartmentCode() {
			return DepartmentCode;
		}

		public void setDepartmentCode(String departmentCode) {
			DepartmentCode = departmentCode;
		}

		public String getDefaultServerId() {
			return DefaultServerId;
		}

		public void setDefaultServerId(String defaultServerId) {
			DefaultServerId = defaultServerId;
		}

		public List<HappyHours> getHappyHours() {
			return HappyHours;
		}

		public void setHappyHours(List<HappyHours> happyHours) {
			HappyHours = happyHours;
		}

		public List<TaxVatDefinitions> getTaxVatDefinitions() {
			return TaxVatDefinitions;
		}

		public void setTaxVatDefinitions(List<TaxVatDefinitions> taxVatDefinitions) {
			TaxVatDefinitions = taxVatDefinitions;
		}
	}

	@JsonProperty("OutletMenus")
	private List<OutletMenus> OutletMenus;

	public static class OutletMenus {

		@JsonProperty("MenuCode")
		private String MenuCode;

		@JsonProperty("MenuName")
		private String MenuName;

		@JsonProperty("SubMenus")
		private List<SubMenus> SubMenus;

		public static class SubMenus {

			@JsonProperty("SubMenuCode")
			private String SubMenuCode;

			@JsonProperty("SubMenuName")
			private String SubMenuName;

			@JsonProperty("MenuItems")
			private List<MenuItems> MenuItems;

			public static class MenuItems {

				@JsonProperty("ItemCode")
				private String ItemCode;

				@JsonProperty("ItemName")
				private String ItemName;

				@JsonProperty("ItemDescription")
				private String ItemDescription;

				@JsonProperty("ItemPrice")
				private BigDecimal ItemPrice;

				@JsonProperty("SubMenuCode")
				private String SubMenuCode;

				@JsonProperty("ItemHappyHourPrice")
				private BigDecimal ItemHappyHourPrice;

				@JsonProperty("ItemType")
				private String ItemType;

				@JsonProperty("ItemImageUrl")
				private String ItemImageUrl;

				@JsonProperty("ItemPotentialPrice")
				private BigDecimal ItemPotentialPrice;

				@JsonProperty("ItemAccount")
				private String ItemAccount;

				@JsonProperty("AddOnItems")
				private List<AddOnItems> AddOnItems;

				public static class AddOnItems {

					@JsonProperty("AddOnCode")
					private String AddOnCode;

					@JsonProperty("AddOnName")
					private String AddOnName;

					@JsonProperty("AddOnType")
					private String AddOnType;

					@JsonProperty("AddOnPrice")
					private BigDecimal AddOnPrice;

					@JsonProperty("AddOnHappyHourPrice")
					private BigDecimal AddOnHappyHourPrice;

					@JsonProperty("AddOnPotentialPrice")
					private BigDecimal AddOnPotentialPrice;

					@JsonProperty("AddOnAccount")
					private String AddOnAccount;

					public String getAddOnCode() {
						return AddOnCode;
					}

					public void setAddOnCode(String addOnCode) {
						AddOnCode = addOnCode;
					}

					public String getAddOnName() {
						return AddOnName;
					}

					public void setAddOnName(String addOnName) {
						AddOnName = addOnName;
					}

					public String getAddOnType() {
						return AddOnType;
					}

					public void setAddOnType(String addOnType) {
						AddOnType = addOnType;
					}

					public BigDecimal getAddOnPrice() {
						return AddOnPrice;
					}

					public void setAddOnPrice(BigDecimal addOnPrice) {
						AddOnPrice = addOnPrice;
					}

					public BigDecimal getAddOnHappyHourPrice() {
						return AddOnHappyHourPrice;
					}

					public void setAddOnHappyHourPrice(BigDecimal addOnHappyHourPrice) {
						AddOnHappyHourPrice = addOnHappyHourPrice;
					}

					public BigDecimal getAddOnPotentialPrice() {
						return AddOnPotentialPrice;
					}

					public void setAddOnPotentialPrice(BigDecimal addOnPotentialPrice) {
						AddOnPotentialPrice = addOnPotentialPrice;
					}

					public String getAddOnAccount() {
						return AddOnAccount;
					}

					public void setAddOnAccount(String addOnAccount) {
						AddOnAccount = addOnAccount;
					}
				}

				@JsonProperty("MenuItemClassifiers")
				private List<MenuItemClassifiers> MenuItemClassifiers;

				public static class MenuItemClassifiers {

					@JsonProperty("ClassifierCode")
					private String ClassifierCode;

					@JsonProperty("ClassifierName")
					private String ClassifierName;

					@JsonProperty("ClassifierImageUrl")
					private String ClassifierImageUrl;

					public String getClassifierCode() {
						return ClassifierCode;
					}

					public void setClassifierCode(String classifierCode) {
						ClassifierCode = classifierCode;
					}

					public String getClassifierName() {
						return ClassifierName;
					}

					public void setClassifierName(String classifierName) {
						ClassifierName = classifierName;
					}

					public String getClassifierImageUrl() {
						return ClassifierImageUrl;
					}

					public void setClassifierImageUrl(String classifierImageUrl) {
						ClassifierImageUrl = classifierImageUrl;
					}

				}

				@JsonProperty("PackageConstituents")
				private List<PackageConstituents> PackageConstituents;

				public static class PackageConstituents {

				}

				public String getItemCode() {
					return ItemCode;
				}

				public void setItemCode(String itemCode) {
					ItemCode = itemCode;
				}

				public String getItemName() {
					return ItemName;
				}

				public void setItemName(String itemName) {
					ItemName = itemName;
				}

				public String getItemDescription() {
					return ItemDescription;
				}

				public void setItemDescription(String itemDescription) {
					ItemDescription = itemDescription;
				}

				public BigDecimal getItemPrice() {
					return ItemPrice;
				}

				public void setItemPrice(BigDecimal itemPrice) {
					ItemPrice = itemPrice;
				}

				public String getSubMenuCode() {
					return SubMenuCode;
				}

				public void setSubMenuCode(String subMenuCode) {
					SubMenuCode = subMenuCode;
				}

				public BigDecimal getItemHappyHourPrice() {
					return ItemHappyHourPrice;
				}

				public void setItemHappyHourPrice(BigDecimal itemHappyHourPrice) {
					ItemHappyHourPrice = itemHappyHourPrice;
				}

				public String getItemType() {
					return ItemType;
				}

				public void setItemType(String itemType) {
					ItemType = itemType;
				}

				public String getItemImageUrl() {
					return ItemImageUrl;
				}

				public void setItemImageUrl(String itemImageUrl) {
					ItemImageUrl = itemImageUrl;
				}

				public BigDecimal getItemPotentialPrice() {
					return ItemPotentialPrice;
				}

				public void setItemPotentialPrice(BigDecimal itemPotentialPrice) {
					ItemPotentialPrice = itemPotentialPrice;
				}

				public String getItemAccount() {
					return ItemAccount;
				}

				public void setItemAccount(String itemAccount) {
					ItemAccount = itemAccount;
				}

				public List<AddOnItems> getAddOnItems() {
					return AddOnItems;
				}

				public void setAddOnItems(List<AddOnItems> addOnItems) {
					AddOnItems = addOnItems;
				}

				public List<MenuItemClassifiers> getMenuItemClassifiers() {
					return MenuItemClassifiers;
				}

				public void setMenuItemClassifiers(List<MenuItemClassifiers> menuItemClassifiers) {
					MenuItemClassifiers = menuItemClassifiers;
				}

				public List<PackageConstituents> getPackageConstituents() {
					return PackageConstituents;
				}

				public void setPackageConstituents(List<PackageConstituents> packageConstituents) {
					PackageConstituents = packageConstituents;
				}

			}

			public String getSubMenuCode() {
				return SubMenuCode;
			}

			public void setSubMenuCode(String subMenuCode) {
				SubMenuCode = subMenuCode;
			}

			public String getSubMenuName() {
				return SubMenuName;
			}

			public void setSubMenuName(String subMenuName) {
				SubMenuName = subMenuName;
			}

			public List<MenuItems> getMenuItems() {
				return MenuItems;
			}

			public void setMenuItems(List<MenuItems> menuItems) {
				MenuItems = menuItems;
			}

		}

		public String getMenuCode() {
			return MenuCode;
		}

		public void setMenuCode(String menuCode) {
			MenuCode = menuCode;
		}

		public String getMenuName() {
			return MenuName;
		}

		public void setMenuName(String menuName) {
			MenuName = menuName;
		}

		public List<SubMenus> getSubMenus() {
			return SubMenus;
		}

		public void setSubMenus(List<SubMenus> subMenus) {
			SubMenus = subMenus;
		}
	}

	@JsonProperty("OutletTables")
	private List<OutletTables> OutletTables;

	public static class OutletTables {

		@JsonProperty("TableNumber")
		private String TableNumber;

		@JsonProperty("Covers")
		private String Covers;

		public String getTableNumber() {
			return TableNumber;
		}

		public void setTableNumber(String tableNumber) {
			TableNumber = tableNumber;
		}

		public String getCovers() {
			return Covers;
		}

		public void setCovers(String covers) {
			Covers = covers;
		}
	}

	@JsonProperty("OutletItemClassifiers")
	private List<OutletItemClassifiers> OutletItemClassifiers;

	public static class OutletItemClassifiers {

	}

	@JsonProperty("ResponseStatus")
	private ResponseStatus responseStatus;

	public ResponseStatus getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(ResponseStatus responseStatus) {
		this.responseStatus = responseStatus;
	}

	public static class ResponseStatus {

		@JsonProperty("ResponseCode")
		private String responseCode;

		@JsonProperty("ResponseFlag")
		private String responseFlag;

		@JsonProperty("ResponseMessage")
		private String responseMessage;

		public String getResponseCode() {
			return responseCode;
		}

		public void setResponseCode(String responseCode) {
			this.responseCode = responseCode;
		}

		public String getResponseFlag() {
			return responseFlag;
		}

		public void setResponseFlag(String responseFlag) {
			this.responseFlag = responseFlag;
		}

		public String getResponseMessage() {
			return responseMessage;
		}

		public void setResponseMessage(String responseMessage) {
			this.responseMessage = responseMessage;
		}

	}

	public OutletConfiguration getOutletConfiguration() {
		return OutletConfiguration;
	}

	public void setOutletConfiguration(OutletConfiguration outletConfiguration) {
		OutletConfiguration = outletConfiguration;
	}

	public List<OutletTables> getOutletTables() {
		return OutletTables;
	}

	public void setOutletTables(List<OutletTables> outletTables) {
		OutletTables = outletTables;
	}

	public List<OutletItemClassifiers> getOutletItemClassifiers() {
		return OutletItemClassifiers;
	}

	public void setOutletItemClassifiers(List<OutletItemClassifiers> outletItemClassifiers) {
		OutletItemClassifiers = outletItemClassifiers;
	}

	public List<OutletMenus> getOutletMenus() {
		return OutletMenus;
	}

	public void setOutletMenus(List<OutletMenus> outletMenus) {
		OutletMenus = outletMenus;
	}

}
